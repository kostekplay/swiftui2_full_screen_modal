////  ContentView.swift
//  SwiftUI2_FullScreenModal
//
//  Created on 17/02/2021.
//  
//

import SwiftUI

struct ContentView: View {
    
    @State private var show = false
    
    var body: some View {
        
        VStack {
            Button("Full Screen Modal") {
                show.toggle()
            }
        }.fullScreenCover(isPresented: $show, content: {
            ZStack(alignment: .topLeading, content: {
                Color.red
                
                Button(action: {
                    show.toggle()
                }, label: {
                    Text("X")
                        .padding()
                })
            })
        })
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
