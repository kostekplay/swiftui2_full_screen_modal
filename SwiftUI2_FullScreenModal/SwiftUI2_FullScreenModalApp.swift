////  SwiftUI2_FullScreenModalApp.swift
//  SwiftUI2_FullScreenModal
//
//  Created on 17/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_FullScreenModalApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
